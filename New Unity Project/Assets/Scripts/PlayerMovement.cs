﻿using UnityEngine;

namespace Assets.Scripts
{
    [RequireComponent(typeof(Rigidbody))]
    public class PlayerMovement : MonoBehaviour
    {

        public Vector3 jump;
        public float jumpForce = 6f;

        public bool isGrounded;
        private Rigidbody rigidbody;

        void Start()
        {
            rigidbody = GetComponent<Rigidbody>();
            jump = new Vector3(0.0f,2.0f,0.0f);
        }

        void OnCollisionStay()
        {
            isGrounded = true;
        }

        void OnCollisionEnter(Collision collision)
        {
            //Stop rotating
            rigidbody.angularVelocity = Vector3.zero;
        }


        // Update is called once per frame
        void Update () {
            var x = Input.GetAxis("Horizontal") * Time.deltaTime * 300.0f;
            var z = Input.GetAxis("Vertical") * Time.deltaTime * 6.0f;

            transform.Rotate(0, x, 0);
            transform.Translate(0, 0, z);

            if (Input.GetKeyDown(KeyCode.Space) && isGrounded)
            {

                rigidbody.AddForce(jump * jumpForce, ForceMode.Impulse);
                isGrounded = false;
            }
        }
    }
}
